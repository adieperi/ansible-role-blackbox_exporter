# ansible-role-blackbox_exporter

Ansible role for deploying the Blackbox Exporter binary for Prometheus

## How to install
### requirements.yml
**Put the file in your roles directory**
```yaml
---
- src: https://gitlab.com/adieperi/ansible-role-blackbox_exporter.git
  scm: git
  version: main
  name: ansible-role-blackbox_exporter
```

### Download the role

```Shell
ansible-galaxy install -f -r ./roles/requirements.yml --roles-path=./roles
```

## Requirements

- Ansible >= 2.10 **(No tests has been realized before this version)**

## Role Variables

All variables which can be overridden are stored in [default/main.yml](default/main.yml) file as well as in table below.

| Name           | Default Value | Choices | Description                        |
| -------------- | ------------- | ------- | -----------------------------------|
| `blackbox_exporter_version` | latest | [version](https://github.com/prometheus/blackbox_exporter/tags) | blackbox_exporter version. |
| `blackbox_exporter_log_level` | info | debug, info, warn, error | blackbox_exporter log level. |
| `blackbox_exporter_log_format` | logfmt | logfmt, json | blackbox_exporter log format. |
| `blackbox_exporter_listen_address` | ':9115' |  | Address to listen on for UI, API, and telemetry. |
| `blackbox_exporter_external_url` | 'http://{{ ansible_default_ipv4.address }}{{ blackbox_exporter_listen_address }}' |  | The URL under which blackbox_exporter is externally reachable. |
| `blackbox_exporter_extra_opts` | [] |  | blackbox_exporter extra options. |
| `blackbox_exporter_configuration` | [] |  | blackbox_exporter [configuration](https://prometheus.io/docs/prometheus/latest/configuration/configuration/) |
| `blackbox_exporter_web_configuration` | [] |  | blackbox_exporter [web configuration](https://prometheus.io/docs/prometheus/latest/configuration/https/). |

## Example Playbook

```yaml
---
- hosts: all
  tasks:
    - name: Include ansible-role-blackbox_exporter
      include_role:
        name: ansible-role-blackbox_exporter
      vars:
        blackbox_exporter_version: '0.21.1'

        blackbox_exporter_configuration:
          modules:
            http_2xx:
              prober: http
              http:
                preferred_ip_protocol: ip4

        blackbox_exporter_web_configuration:
          tls_server_config:
            cert_file: /etc/ssl/prometheus.mydomain.net.crt
            key_file: /etc/ssl/prometheus.mydomain.net.key
```
## License

This project is licensed under MIT License. See [LICENSE](/LICENSE) for more details.

## Maintainers and Contributors

- [Anthony Dieperink](https://gitlab.com/adieperi)
